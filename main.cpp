#include <iostream>

using namespace std;

typedef int feet;
enum status { inactive, active } user_status;
#define NEW_LINE '\n'

int main(int argc, char const *argv[])
{
	/* code */
	cout << "Headfirst CPP" << endl;
	cout << "Size of bool:" << sizeof(bool) << endl;
	cout << "Size of char:" << sizeof(char) << endl;
	cout << "Size of int:" << sizeof(int) << endl;
	cout << "Size of long:" << sizeof(long) << endl;
	cout << "Size of float:" << sizeof(float) << endl;
	cout << "Size of double:" << sizeof(double) << endl;
	cout << "Size of wchar_t:" << sizeof(wchar_t) << endl;
	feet distance;
	distance = 10;
	cout << "distance:" << distance << endl;
	user_status = active;
	cout << user_status << endl;
	cout << "a" << NEW_LINE << "b" << NEW_LINE;
	return 0;
}